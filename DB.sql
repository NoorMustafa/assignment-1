﻿Use Master
GO
CREATE DATABASE WebSiteVisitsDB
GO
USE WebSiteVisitsDB
GO
CREATE TABLE WebSiteVisits
(
    WeekDate DATETIME,
    WebSite VARCHAR(500),
    VisitCount BIGINT
)
GO
CREATE TABLE [dbo].[WebSiteVisits]
(
	Id INT NOT NULL PRIMARY KEY,
	DATETIME,
    WebSite VARCHAR(500),
    VisitCount BIGINT
)
GO
INSERT INTO [dbo].[WebSiteVisits] VALUES (1,'2016-01-06','www.bing.com',14065457);
INSERT INTO [dbo].[WebSiteVisits] VALUES (2,'2016-01-06','www.ebay.com.au',19831166);
INSERT INTO [dbo].[WebSiteVisits] VALUES (3,'2016-01-06','www.facebook.com',104346720);
INSERT INTO [dbo].[WebSiteVisits] VALUES (4,'2016-01-06','mail.live.com',21536612);
INSERT INTO [dbo].[WebSiteVisits] VALUES (5,'2016-01-06','www.wikipedia.org',13246531);
INSERT INTO [dbo].[WebSiteVisits] VALUES (6,'2016-01-27','www.ebay.com.au',23154653);
INSERT INTO [dbo].[WebSiteVisits] VALUES (7,'2016-01-06','au.yahoo.com',11492756);
INSERT INTO [dbo].[WebSiteVisits] VALUES (8,'2016-01-06','www.google.com',26165099);
INSERT INTO [dbo].[WebSiteVisits] VALUES (9,'2016-01-13','www.youtube.com',68487810);
INSERT INTO [dbo].[WebSiteVisits] VALUES (10,'2016-01-27','www.wikipedia.org',16550230);
INSERT INTO [dbo].[WebSiteVisits] VALUES (11,'2016-01-06','ninemsn.com.au',21734381);
INSERT INTO [dbo].[WebSiteVisits] VALUES (12,'2016-01-20','mail.live.com',24344783);
INSERT INTO [dbo].[WebSiteVisits] VALUES (13,'2016-01-20','www.ebay.com.au',22598506);
INSERT INTO [dbo].[WebSiteVisits] VALUES (14,'2016-01-27','mail.live.com',24272437);
INSERT INTO [dbo].[WebSiteVisits] VALUES (15,'2016-01-27','www.bing.com',16041776);
INSERT INTO [dbo].[WebSiteVisits] VALUES (16,'2016-01-20','ninemsn.com.au',24241574);
INSERT INTO [dbo].[WebSiteVisits] VALUES (17,'2016-01-20','www.facebook.com',118984483);
INSERT INTO [dbo].[WebSiteVisits] VALUES (18,'2016-01-27','ninemsn.com.au',24521168);
INSERT INTO [dbo].[WebSiteVisits] VALUES (19,'2016-01-27','www.facebook.com',123831275);
INSERT INTO [dbo].[WebSiteVisits] VALUES (20,'2016-01-20','www.bing.com',16595739);
INSERT INTO [dbo].[WebSiteVisits] VALUES (21,'2016-01-13','www.facebook.com',118506019);
INSERT INTO [dbo].[WebSiteVisits] VALUES (22,'2016-01-20','www.google.com.au',170020924);
INSERT INTO [dbo].[WebSiteVisits] VALUES (23,'2016-01-27','www.youtube.com',69327140);
INSERT INTO [dbo].[WebSiteVisits] VALUES (24,'2016-01-13','mail.live.com',24772355);
INSERT INTO [dbo].[WebSiteVisits] VALUES (25,'2016-01-13','ninemsn.com.au',24555033);
INSERT INTO [dbo].[WebSiteVisits] VALUES (26,'2016-01-20','www.google.com',28996455);
INSERT INTO [dbo].[WebSiteVisits] VALUES (27,'2016-01-13','www.bing.com',16618315);
INSERT INTO [dbo].[WebSiteVisits] VALUES (28,'2016-01-27','www.google.com.au',171842376);
INSERT INTO [dbo].[WebSiteVisits] VALUES (29,'2016-01-06','www.youtube.com',59811438);
INSERT INTO [dbo].[WebSiteVisits] VALUES (30,'2016-01-13','www.netbank.commbank.com.au',13316233);
INSERT INTO [dbo].[WebSiteVisits] VALUES (31,'2016-01-20','www.netbank.commbank.com.au',13072234);
INSERT INTO [dbo].[WebSiteVisits] VALUES (32,'2016-01-13','www.ebay.com.au',22785028);
INSERT INTO [dbo].[WebSiteVisits] VALUES (33,'2016-01-20','www.wikipedia.org',16519992);
INSERT INTO [dbo].[WebSiteVisits] VALUES (34,'2016-01-27','www.bom.gov.au',14369775);
INSERT INTO [dbo].[WebSiteVisits] VALUES (35,'2016-01-27','www.google.com',29422150);
INSERT INTO [dbo].[WebSiteVisits] VALUES (36,'2016-01-20','www.youtube.com',69064107);
INSERT INTO [dbo].[WebSiteVisits] VALUES (37,'2016-01-06','www.google.com.au',151749278);
INSERT INTO [dbo].[WebSiteVisits] VALUES (38,'2016-01-13','www.wikipedia.org',16015926);
INSERT INTO [dbo].[WebSiteVisits] VALUES (39,'2016-01-13','www.google.com',29203671);
INSERT INTO [dbo].[WebSiteVisits] VALUES (40,'2016-01-13','www.google.com.au',172220397);
GO