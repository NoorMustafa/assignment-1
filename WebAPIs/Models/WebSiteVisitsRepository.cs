﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.SqlServer;

namespace WebAPIs.Models
{
    public class WebSiteVisitsRepository
    {
        private static masterEntities dataContext = new masterEntities();

        public static List<WebSiteVisit> GetAllWebSiteVists()
        {
            var query = from visit in dataContext.WebSiteVisits
                        select visit;
            return query.ToList();
        }

        public static List<WebSiteVisit> GetTopXWebSiteVists(DateTime weekDate, int recordCountToReturn)
        {
            var query = (from visit in dataContext.WebSiteVisits
                         where SqlFunctions.DateDiff("DAY", visit.WeekDate, weekDate)==0
                        orderby visit.VisitCount descending
                        select visit).Take(recordCountToReturn);

            return query.ToList();
        }

        
    }
}