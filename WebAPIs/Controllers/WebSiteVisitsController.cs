﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPIs.Models;
using System.Web.Http.Cors;

namespace WebAPIs.Controllers
{
    [EnableCors(origins: "http://localhost:61519", headers: "*", methods: "*")]
    public class WebSiteVisitsController : ApiController
    {
        // GET: api/WebSiteVisits
        [Route("api/WebSiteVisits")]
        public IEnumerable<WebSiteVisit> Get()
        {
            //string date = "01/06/2016";
            //DateTime dt = Convert.ToDateTime(date);
            //var employees = WebSiteVisitsRepository.GetTopXWebSiteVists(dt, 5);
            return null;
        }

        [Route("api/WebSiteVisits/{weekdate}")]
        public IEnumerable<WebSiteVisit> Get(DateTime weekdate)
        {
            var employees = WebSiteVisitsRepository.GetTopXWebSiteVists(weekdate, 5);
            return employees;
        }


       
      
    }
}
