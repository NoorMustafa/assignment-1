﻿var app = angular.module('WebSiteVisitsModule', ['ngRoute', 'ui.bootstrap']);

app.config(function ($routeProvider) {

    $routeProvider

        .when("/Home", {

            templateUrl: "/Home.html",

            controller: "HomeController"

        })

        .when("/WebSiteVisits", {

            templateUrl: "WebSiteVisits/WebSiteVisits.html",

            controller: "WebSiteVisitsController"

        })

        .otherwise({ redirectTo: "/Home" })

});