﻿(function () {
    var visitsService = function ($http) {
        var visits = function () {
            return $http.get("http://localhost:52071/api/WebSiteVisits")
                .then(function (serviceResp) {
                    return serviceResp.data;
                });
        };
        var searchVisits = function (weekdate) {
            return $http.get("http://localhost:52071/api/WebSiteVisits/" + weekdate)
                .then(function (serviceResp) {
                    return serviceResp.data;
                });
        };
        return {
            visits: visits,
            searchVisits: searchVisits
        };
    };
    var module = angular.module("WebSiteVisitsModule");
    module.factory("visitsService", ["$http", visitsService]);
}());