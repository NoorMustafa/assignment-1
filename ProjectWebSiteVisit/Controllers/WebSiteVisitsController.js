﻿(function () {
    var WebSiteVisitsController = function ($scope, visitsService, $log) {
        var visits = function (data) {
            $scope.Visits = data;
        };
        $scope.searchVisits = function (weekdate) {
            visitsService.searchVisits(weekdate)
                .then(visits, errorDetails);
            $log.info("Found visits for week - " + weekdate);
        };
        var errorDetails = function (serviceResp) {
            $scope.Error = "Something went wrong ??";
        };
        visitsService.visits().then(visits, errorDetails);
        $scope.Title = "Visits Details Page";
        $scope.weekdate = null;
    };
    app.controller("WebSiteVisitsController", ["$scope", "visitsService", "$log", WebSiteVisitsController]);
}());